﻿
namespace MaterialManagerModels
{
    public class Material
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public bool IsDeleted { get; set; }
    }
}
