﻿using MaterialManagerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialManagerInterfaces.Services
{
    public interface IMaterialsService
    {
        List<Material> GetMaterials();
        List<Material> AddMaterial(Material material);
        bool UpdateMaterial(Material material);
        Material GetMaterialById(int id);
    }
}
