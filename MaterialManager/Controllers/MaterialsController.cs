using System;
using System.Collections.Generic;
using System.Linq;
using MaterialManagerInterfaces.Services;
using MaterialManagerModels;
using Microsoft.AspNetCore.Mvc;

namespace MaterialManager.Controllers
{
    [Route("api/[controller]")]
    public class MaterialsController : Controller
    {
        private readonly IMaterialsService _materialService;

        public MaterialsController(IMaterialsService materialService)
        {
            _materialService = materialService;
        }

        [HttpGet("[action]")]
        public IEnumerable<Material> GetMaterials()
        {
            return _materialService.GetMaterials();
        }

        [HttpGet]
        [Route("[action]/{id}")]
        public Material GetMaterial(int id)
        {
            return _materialService.GetMaterialById(id);
        }

        [HttpPost]
        [Route("[action]")]
        public bool UpdateMaterial([FromBody]Material material)
        {
            return _materialService.UpdateMaterial(material);
        }
    }
}
