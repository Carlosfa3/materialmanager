import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { MaterialsComponent } from './materials/Components/materials.component';
import { MaterialDetailsComponent } from './materials/Components/material-details.component';

import { MaterialsService } from './materials/Services/materials.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    MaterialsComponent,
    MaterialDetailsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'materials', component: MaterialsComponent },
      { path: 'material/:id', component: MaterialDetailsComponent }
    ])
  ],
  providers: [MaterialsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
