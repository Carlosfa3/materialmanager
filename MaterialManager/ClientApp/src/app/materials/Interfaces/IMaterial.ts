
export interface IMaterial {
  id: number;
  name: string;
  quantity: number;
  isDeleted: boolean;
}
