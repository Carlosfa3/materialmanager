import { Component } from '@angular/core';
import { IMaterial } from '../Interfaces/IMaterial';
import { MaterialsService } from '../Services/materials.service';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html'
})
export class MaterialsComponent {
  public materials: IMaterial[];

  constructor(private materialsService: MaterialsService)
  {
    this.materialsService.getMaterials()
      .subscribe(materials => { this.materials = materials; }
        , error => console.error(error));
  }
}
