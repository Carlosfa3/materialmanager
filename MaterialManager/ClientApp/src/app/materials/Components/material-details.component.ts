import { Component } from '@angular/core';
import { IMaterial } from '../Interfaces/IMaterial';
import { MaterialsService } from '../Services/materials.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-material-details',
  templateUrl: './material-details.component.html'
})
export class MaterialDetailsComponent {
  public id: number;
  public material: IMaterial;

  constructor(private materialsService: MaterialsService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      console.log(params.id);
      this.id = params.id;
    });

    this.materialsService.getMaterial(this.id).subscribe(material => this.material = material);
  }

  update() {
    this.materialsService.updateMaterial(this.material).subscribe(res => {
      if (res) {
        console.log('Success');
      } else {
        console.log('Failure');
      }
    });
  }
}

