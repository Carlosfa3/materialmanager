import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMaterial } from '../Interfaces/IMaterial';

@Injectable()
export class MaterialsService {
  public material: IMaterial;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    
  }

  public getMaterials() {
    return this.http.get<IMaterial[]>(this.baseUrl + 'api/Materials/GetMaterials');
  }

  public getMaterial(id: number) {
    return this.http.get<IMaterial>(this.baseUrl + 'api/Materials/GetMaterial/'+id);
  }

  public updateMaterial(material: IMaterial) {
    return this.http.post<boolean>(this.baseUrl + 'api/Materials/UpdateMaterial', material);
  }
}

