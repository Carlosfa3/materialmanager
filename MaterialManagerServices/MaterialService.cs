﻿using MaterialManagerInterfaces.Services;
using MaterialManagerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialManagerServices
{
    public class MaterialService : IMaterialsService
    {
        //Our fake materials
        private static readonly List<Material> Materials = new List<Material>() {
            new Material()
            {
                Id = 0,
                Name = "Cement",
                Quantity = 1000,
                IsDeleted = false
            },
            new Material()
            {
                Id = 1,
                Name = "Wood",
                Quantity = 750,
                IsDeleted = false
            }
        };

        public List<Material> AddMaterial(Material material)
        {
            material.Id = Materials.Count + 1;
            Materials.Add(material);
            return Materials;
        }

        public Material GetMaterialById(int id)
        {
            var index = Materials.FindIndex(m => m.Id == id);
            if (index == -1)
            {
                return null;
            }
            return Materials[index];
        }

        public List<Material> GetMaterials()
        {
            return Materials;           
        }

        public bool UpdateMaterial(Material material)
        {
            var index = Materials.FindIndex(m => m.Id == material.Id);
            if(index == -1)
            {
                return false;
            }
            Materials[index] = material;
            return true;
        }
    }
}
