**Project was built using Angular CLI. See CliantApp for more instructions and package sync**

# Project
This is a simple inventory view and update web application using modenr Angular2 and Visual Studio 2017.
Files are properly grouped and separated in the /ClientApp/src/app/materials folder accoring to their purpose.

Components have our usable components
Services contain any service responsible for communication with the application or further logic.
Interfaces contains any interface most closely relevant to Material. A generic interface used across the app would be stored in shared/interfaces folder under app.

Files are grouped according to their purpose and kept as decoupled as possible.
Components receive providers from the app.module.ts declaration, leaving them easy to inject into for testing.
Htmls are stored close to their ts since the 2 are very closely bound and for ease of development.

# BackEnd
The backend is mostly empty but has the skeleton of how I'd organize the projects. This organization seeks to properly isolate the back-end from our actual app implementation as well as allow
for quick and easy swapping or mocking of elements within each project.

Models should not have any external dependency or logic. It should contain solely DTOs and other data structures.
Interfaces is a specific project which should not contain any concrete classes and depend solely on models.
Services will depend on Models and Interfaces. Within it our app services will be kept.

The front-end DI declarations are responsible for actually putting together interfaces to their concrete implementaions during execution.